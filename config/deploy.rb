require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rvm'
require 'mina/whenever'

set :ruby_version, '2.5.1'

set :repository, 'git@bitbucket.org:giovaneperlin/marvel_test.git'

task :production do
  set :rails_env, 'production'
  set :user, 'marvel_test'
  set :domain, '72.14.181.27'
  set :deploy_to, '/home/marvel_test/public_html/railsapp'
  set :branch, 'deploy'
end

set :term_mode, nil

set :shared_dirs, fetch(:shared_dirs, []).push('log', 'tmp')
set :shared_files, fetch(:shared_files, []).push('config/application.yml', 'config/secrets.yml')
set :shared_path, -> { "#{fetch(:deploy_to)}/shared" }

task :environment do
  invoke :"rvm:use", "2.5.1@marvel_test"
end

task :setup do
  command %[mkdir -p "/home/marvel_test/public_html/railsapp/shared/log"]
  command %[chmod g+rx,u+rwx "/home/marvel_test/public_html/railsapp/shared/log"]

  command %[mkdir -p "/home/marvel_test/public_html/railsapp/storage"]
  command %[chmod g+rx,u+rwx "/home/marvel_test/public_html/railsapp/storage"]

  command %[touch "/home/marvel_test/public_html/railsapp/storage/index.html"]

  command %[mkdir -p "/home/marvel_test/public_html/railsapp/shared/config"]
  command %[chmod g+rx,u+rwx "/home/marvel_test/public_html/railsapp/shared/config"]

  command %[mkdir -p "/home/marvel_test/public_html/railsapp/shared/pids"]
  command %[chmod g+rx,u+rwx "/home/marvel_test/public_html/railsapp/shared/pids"]

  command %[mkdir -p "/home/marvel_test/public_html/railsapp/shared/tmp"]
  command %[chmod g+rx,u+rwx "/home/marvel_test/public_html/railsapp/shared/tmp"]

  command %[touch "/home/marvel_test/public_html/railsapp/shared/config/application.yml"]
  command  %[echo "-----> Be sure to edit 'shared/config/application.yml'."]

  command %[touch "/home/marvel_test/public_html/railsapp/shared/config/secrets.yml"]
  command  %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:assets_precompile'

    on :launch do
      command %[echo -n "-----> Creating new restart.txt: "]
      command "touch /home/marvel_test/public_html/railsapp/shared/tmp/restart.txt"
    end
  end
end

desc "Rolls back the latest release"
task :rollback do
  command %[echo "-----> Rolling back to previous release for instance: 72.14.181.27"]
  command %[echo -n "-----> Creating new symlink from the previous release: "]
  command %[ls "/home/marvel_test/public_html/railsapp/releases" -Art | sort | tail -n 2 | head -n 1]
  command %[ls -Art "/home/marvel_test/public_html/railsapp/releases" | sort | tail -n 2 | head -n 1 | xargs -I active ln -nfs "/home/marvel_test/public_html/railsapp/releases/active" "/home/marvel_test/public_html/railsapp/current"]

  command %[echo -n "-----> Deleting active release: "]
  command %[ls "/home/marvel_test/public_html/railsapp/releases" -Art | sort | tail -n 1]
  command %[ls "/home/marvel_test/public_html/railsapp/releases" -Art | sort | tail -n 1 | xargs -I active rm -rf "/home/marvel_test/public_html/railsapp/releases/active"]

  command %[echo -n "-----> Creating new restart.txt: "]
  command "touch /home/marvel_test/public_html/railsapp/shared/tmp/restart.txt"
end

desc "TurnOff"
task :'system:turnoff' do
  command %[echo -n "-----> Turn Off System: "]
  command %[cd "/home/marvel_test/public_html/railsapp/current"]
  command "RAILS_ENV=production bundle exec rake maintenance:start"
end

desc "TurnOn"
task :'system:turnon' do
  command %[echo -n "-----> Turn Off System: "]
  command %[cd "/home/marvel_test/public_html/railsapp/current"]
  command "RAILS_ENV=production bundle exec rake maintenance:end"
end
