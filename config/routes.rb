Rails.application.routes.draw do
  root :to => redirect("/herois/page/0")

  get "/herois/page/:page/" => "heroes#index", as: :heroes
  get "/herois/id/:id/" => "heroes#show", as: :hero
  get "/herois/id/:id/get_comics/:comics_page/" => "heroes#get_comics", as: :get_comics
end
