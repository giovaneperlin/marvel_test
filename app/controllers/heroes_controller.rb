class HeroesController < ApplicationController
  def index
    page = params[:page].present? ? params[:page].to_i : 0

    @request = api_requester("https://gateway.marvel.com/v1/public/characters", { limit: 50, offset: page * 50 })

    @page_title = "Heŕois - MarvelTest"

    unless @request.code != 200
      @request = JSON.parse(@request.body)["data"]
      @pages = @request["total"].to_i / 50
    else
      redirect_to "/500"
    end
  end

  def show
    id = params[:id].present? ? params[:id].to_i : 0

    @request = api_requester("https://gateway.marvel.com/v1/public/characters/#{id}", {})

    unless @request.code != 200
      @request = JSON.parse(@request.body)["data"]
      @page_title = "#{@request["results"][0]["name"]} - MarvelTest"
      @page_image = @request["results"][0]["thumbnail"]["path"] + "/standard_fantastic." + @request["results"][0]["thumbnail"]["extension"]

      @comics = []
      @request["results"].each do |result|
        result["comics"]["items"][0..9].each do |comic|
          comic = api_requester(comic["resourceURI"], {})
          comic = JSON.parse(comic.body)["data"]["results"][0]
          @comics = @comics.push(comic)
        end
      end
    else
      redirect_to "/404"
    end
  end

  def get_comics
    id = params[:id].present? ? params[:id].to_i : 0
    comics_page = params[:comics_page].present? ? params[:comics_page].to_i : 0

    @request = api_requester("https://gateway.marvel.com/v1/public/characters/#{id}/comics", { limit: 10, offset: comics_page * 10 })
    unless @request.code != 200
      @comics = JSON.parse(@request.body)["data"]["results"]

      if @comics.size != 0
        render status: 200, json: @comics.to_json
      else
        render status: 204, json: { message: "204 No Content" }.to_json
      end
    else
      render status: 404, json: { message: "404 Page Not Found" }.to_json
    end
  end
end
