class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def api_requester(url, params)
    params[:ts] = Time.now.to_s
    params[:apikey] = YAML.load_file("config/application.yml")["marvel_api_public_key"]
    params[:hash] = Digest::MD5.new.hexdigest(
          Time.now.to_s +
          YAML.load_file("config/application.yml")["marvel_api_private_key"] +
          YAML.load_file("config/application.yml")["marvel_api_public_key"]
    )

    return response = Typhoeus.get(
      url,
      params: params
    )
  end
end
