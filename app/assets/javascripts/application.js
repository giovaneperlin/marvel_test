// This is a manifest file that"ll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin"s vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It"s not advisable to add code directly here, but if you do, it"ll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

document.addEventListener("turbolinks:load", function() {
  $(document).ready(function() {
    var path = window.location.pathname.split("/");
    var url = "";

    for (var i = 1; i < path.length; i++) {
      url += "/" + path[i];

      $('a[href="'+ url +'"]').addClass("current--page");
    }
  });

  $(document).ready(function() {
    $("#load_comics").each(function() {
        $(this).on("click", function() {
            var btn = this;
            var page = Number.parseInt(btn.dataset.page) + 1;
            if (typeof page == "number") {
              var path = window.location.pathname;
              if (path[path.length - 1] == "/") {
                path = path.substr(0, path.length -1);
              }

              $.get(path + "/get_comics/" + page, {}).done(function(data, textStatus, jqXHR) {
                  if (jqXHR.status == 200) {
                    var comics = $(".superhero__comics")[0];
                    $(data).each(function() {
                      var item = document.createElement("div");
                      $(item).addClass("superhero__comics--item");

                      var name = document.createElement("span");
                      $(name).addClass("superhero__comics--name");
                      name.innerText = this.title;

                      var figure = document.createElement("figure");
                      $(figure).addClass("superhero__comics--figure");

                      var image = document.createElement("img");
                      $(image).addClass("superhero__comics--image");
                      image.src = this.thumbnail.path + "/portrait_uncanny." + this.thumbnail.extension;

                      item.appendChild(name);
                      figure.appendChild(image);
                      item.appendChild(figure);
                      comics.appendChild(item);
                    });

                    btn.dataset.page = page;
                  } else {
                    $(btn).remove();
                  }
              }).fail(function() {
                  $(btn).text("Erro!");
                  setTimeout(function(){
                    $(btn).text("Carregar mais +");
                  }, 3000);
              });
            }
        });
    });
  });
});
